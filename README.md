# Fastify Starter
A starter project for fastify app with typescript and mongodb.

## NPM Scripts
```sh
## To run app on production server 
npm start

## To run app on development server
npm run dev

## To run linting on app
npm run lint
```


## Tech Stack
* [Node.js](https://nodejs.org/en/) - A JavaScript runtime built on [Chrome's V8 JavaScript engine](https://v8.dev/).
* [Typescript](https://www.typescriptlang.org/) - A typed superset of JavaScript that compiles to plain JavaScript.
* [Fastify](https://fastify.dev/) - Fast and low overhead web framework, for [Node.js](https://nodejs.org/en/).
* [MongoDB](https://www.mongodb.com/) - An open-source document based NoSQL database.
* [Pino](https://github.com/pinojs/pino) - Very low overhead Node.js logger.
* [Joi]() - A Object schema description language and validator for JavaScript objects.


## Features
- :heavy_check_mark: Environment schema validation and uses .env file
- [x] Linting using eslint 
- [x] Formatting using prettier
- [ ] Monitoring using newrelic
- [ ] Health check
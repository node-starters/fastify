import { FastifyInstance } from 'fastify';

export function defineSchema(fastify: FastifyInstance): void {
	fastify.addSchema({
		$id: 'email',
		type: 'string',
		description: 'Email ID',
		maxLength: 320,
		minLength: 5,
	});
}

/* eslint-disable @typescript-eslint/naming-convention */
import 'fastify';
declare module 'fastify' {
	type EnvName = 'local' | 'development' | 'testing' | 'staging' | 'production';
	interface Config {
		readonly NODE_ENV: EnvName;
		readonly HOST: string;
		readonly PORT: number;
		readonly MONGO_URI: string;
		readonly BASIC_AUTH_USER: string;
		readonly BASIC_AUTH_PASS: string;
		readonly AUTH_TOKEN_SECRET: string;
		readonly REFRESH_TOKEN_SECRET: string;
	}
	interface FastifyInstance {
		config: Config;
	}
}

import './polyfill/index.js';
import { app } from './app.js';
import cors from '@fastify/cors';
import fastifyJWT from '@fastify/jwt';
import swagger from '@fastify/swagger';
import swaggerUI from '@fastify/swagger-ui';
import { connect } from 'mongoose';
import { fastifyRouter } from './lib/index.js';
import { ApiRouter } from './api/api.router.js';
import { AdminRouter } from './api/admin/admin.router.js';
import { verifyAuth } from './guards/index.js';
import { Auth } from './lib/auth.js';
import { AccountRouter } from './api/account/account.router.js';

const registers = [
	app.register(cors, {}),
	app.register(fastifyJWT, {
		secret: app.config.AUTH_TOKEN_SECRET,
	}),
	(async (): Promise<void> => {
		const logger = app.log.child({
			name: 'Mongoose',
		});
		try {
			logger.info('Connecting ...');
			const { connection: conn, set } = await connect(app.config.MONGO_URI);
			logger.info('Connected !');
			set('debug', true);
			conn.on('connecting', () => logger.info('Connecting ...'));
			conn.on('connected', () => logger.info('Connected !'));
			conn.on('open', () => logger.info('Open !'));
			conn.on('disconnected', () => logger.info('Disconnected !'));
			conn.on('reconnected', () => logger.info('Re Connected !'));
			conn.on('disconnecting', () => logger.info('Disconnecting ...'));
			conn.on('close', () => logger.info('Closed !'));
		} catch (err) {
			logger.error(err);
			process.exit(0);
		}
	})(),
];

if (app.config.NODE_ENV !== 'production') {
	registers.push(
		app.register(swagger, {
			swagger: {
				info: {
					title: 'My App',
					description: 'APIs documentation for My App',
					version: '0.1.0',
				},
				// externalDocs: {
				// 	url: 'https://swagger.io',
				// 	description: 'Find more info here',
				// },
				// host: 'localhost:3000',
				// schemes: ['http'],

				consumes: ['application/json'],
				produces: ['application/json'],
				tags: [{ name: 'Admin', description: 'Admin Management APIs' }],
				securityDefinitions: {
					basic: Auth.Basic.swagger,
					bearer: Auth.Bearer.swagger,
				},
			},
		}),
		app.register(swaggerUI, {
			routePrefix: '/swagger',
		}),
	);
}

await Promise.all(registers);

app.register(fastifyRouter).after(() => {
	app.router(ApiRouter);
	app.router(AdminRouter);
	app.router(AccountRouter);
});

app.addHook('onRequest', verifyAuth);

await app.listen({
	host: app.config.HOST,
	port: app.config.PORT,
});

// all the database operations

import { AdminModel } from './admin.model.js';

class AdminDAO {
	async bootstrap(): Promise<void> {
		try {
			await AdminModel.create();
		} catch (err) {
			console.error(err);
		}
	}
}

export const adminDAO = new AdminDAO();

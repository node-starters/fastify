import { Schema, model } from 'mongoose';

export const nameSchema = new Schema(
	{
		first: { type: String, required: true },
		last: { type: String, required: true },
	},
	{ _id: false },
);

export const adminSchema = new Schema(
	{
		name: {
			type: nameSchema,
			required: true,
		},
		email: {
			type: String,
			required: true,
			unique: true,
		},
	},
	{
		timestamps: true,
		collection: 'admins',
		autoIndex: true,
	},
);

export class AdminModel extends model('admins', adminSchema) {}

import type { FastifyReply, FastifyRequest } from 'fastify';
// import { promisify } from 'node:util';
// const wait = promisify(setTimeout);

import { Route, Router } from '../../lib/router.js';
import { Auth } from '../../lib/auth.js';

@Router({
	prefix: '/admin',
})
export class AdminRouter {
	@Auth.Bearer()
	@Route.Post({
		schema: {
			summary: 'Create Sub Admin 1',
			tags: ['Admin'],
			body: {
				type: 'object',
				required: ['name'],
				properties: {
					name: {
						type: 'string',
						description: 'Full Name',
						// example: 'Ashish Gurjar1',
					},
				},
			},
		},
	})
	async create(req: FastifyRequest, reply: FastifyReply): Promise<object> {
		console.info(req.body);
		return await reply.send({ id: 'ADM-123' });
	}
}

import { JWT } from '@fastify/jwt';

class AccountService {
	async login(payload: object, jwtUtil: JWT): Promise<object> {
		await Promise.resolve();
		return {
			action: 0,
			authToken: jwtUtil.sign({
				aid: 'asdfghjkl',
			}),
			refreshToken: jwtUtil.sign(
				{
					aid: 'asdfghjkl',
				},
				{
					key: '',
				},
			),
		};
	}
}

export const accountService = new AccountService();

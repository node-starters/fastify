import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
// import { promisify } from 'node:util';
// const wait = promisify(setTimeout);

import { Route, Router } from '../../lib/router.js';
import { Auth } from '../../lib/auth.js';
import { loginValidator } from './account.validators.js';
import { accountService } from './account.service.js';

@Router({
	prefix: '/accounts',
})
export class AccountRouter {
	@Auth.Basic()
	@Route.Post('/login', {
		schema: {
			summary: 'Login into Account',
			tags: ['Account'],
			body: loginValidator.body,
			response: loginValidator.response,
		},
	})
	async login(
		req: FastifyRequest,
		reply: FastifyReply,
		fastify: FastifyInstance,
	): Promise<object> {
		console.info(req.body);
		const result = await accountService.login(req.body as object, fastify.jwt);
		return await reply.send(result);
	}
}

export const loginValidator = {
	body: {
		type: 'object',
		required: ['email', 'password'],
		properties: {
			email: {
				$ref: 'email#',
			},
			password: {
				type: 'string',
				description: 'Password',
			},
		},
	},
	response: {
		[200]: {
			type: 'object',
			required: ['action'],
			properties: {
				action: {
					type: 'number',
					enum: [0, 10, 20],
					title: 'Action (If required)',
					description: '0: None, 10: Setup, 20: Verify',
				},
				authToken: { type: 'string' },
				refreshToken: { type: 'string' },
			},
		},
	},
};

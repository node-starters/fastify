import type { FastifyRequest, FastifyReply, FastifyInstance } from 'fastify';
// import { promisify } from 'node:util';
// const wait = promisify(setTimeout);
// import './admin/admin.router.js';
import { Route, Router } from '../lib/index.js';

@Router()
export class ApiRouter {
	@Route.Get()
	async healthz(
		req: FastifyRequest,
		reply: FastifyReply,
		app: FastifyInstance,
	): Promise<void> {
		app.log.error('Hello World');
		await reply.send('Fine !');
	}
}

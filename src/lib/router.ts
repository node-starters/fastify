/* eslint-disable @typescript-eslint/indent */
/* eslint-disable @typescript-eslint/naming-convention */
import type {
	FastifyInstance,
	FastifyReply,
	FastifyRequest,
	RouteOptions,
} from 'fastify';
import { join } from 'node:path';
import type {
	HandlerOptions,
	IMetadata,
	MethodOptions,
	IRouterOptions,
} from './types.js';
import deepMerge from '@fastify/deepmerge';

const merge = deepMerge({ all: true });

export function SetRouteMetadata(opts: HandlerOptions) {
	return <
		This,
		Args extends [FastifyRequest, FastifyReply, FastifyInstance],
		Return,
	>(
		_: (this: This, ...args: Args) => Return,
		context: ClassMethodDecoratorContext<
			This,
			(this: This, ...args: Args) => Return
		>,
	): void => {
		if (context.kind !== 'method') {
			throw new Error('Route decorator can be applied on method only');
		}
		if (context.private) {
			throw new Error(
				`'bound' cannot decorate private properties like ${context.name as string}.`,
			);
		}
		context.addInitializer(function () {
			if (!context.metadata?.routes) {
				throw new Error('Not a router class');
			}
			const self = this as Record<
				string | symbol,
				(
					req: FastifyRequest,
					reply: FastifyReply,
					app: FastifyInstance,
				) => unknown
			>;
			const route: RouteOptions = {
				...opts,
				childLoggerFactory(logger) {
					return logger.child({ name: self.constructor.name });
				},
				handler(req: FastifyRequest, reply: FastifyReply) {
					return self[context.name](req, reply, this);
				},
			};
			const metadata = context.metadata as unknown as IMetadata;
			if (metadata.prefix && route.url) {
				route.url = join(metadata.prefix, route.url);
			} else if (metadata.prefix) {
				route.url = metadata.prefix;
			} else if (!route.url) {
				route.url = '/';
			}
			if (metadata.routeMap.has(context.name)) {
				const index = metadata.routeMap.get(context.name) as number;
				metadata.routes[index] = merge(metadata.routes[index], route);
			} else {
				metadata.routeMap.set(context.name, metadata.routes.push(route) - 1);
			}
		});
	};
}

type DecoratorFn = ReturnType<typeof SetRouteMetadata>;

export function Get(url: string = '', opts: MethodOptions = {}): DecoratorFn {
	if (typeof url === 'object') {
		opts = url;
		url = '';
	}
	return SetRouteMetadata({ method: 'GET', url, ...opts });
}

export function Put(url: string = '', opts: MethodOptions = {}): DecoratorFn {
	if (typeof url === 'object') {
		opts = url;
		url = '';
	}
	return SetRouteMetadata({ method: 'PUT', url, ...opts });
}

export function Post(
	url: string | MethodOptions = '',
	opts: MethodOptions = {},
): DecoratorFn {
	if (typeof url === 'object') {
		opts = url;
		url = '';
	}
	return SetRouteMetadata({ method: 'POST', url, ...opts });
}

export function Patch(url: string = '', opts: MethodOptions = {}): DecoratorFn {
	if (typeof url === 'object') {
		opts = url;
		url = '';
	}
	return SetRouteMetadata({ method: 'PATCH', url, ...opts });
}

export function Delete(
	url: string = '',
	opts: MethodOptions = {},
): DecoratorFn {
	if (typeof url === 'object') {
		opts = url;
		url = '';
	}
	return SetRouteMetadata({ method: 'DELETE', url, ...opts });
}

interface IRouteFn {
	(opts: HandlerOptions): DecoratorFn;
	Get: typeof Get;
	Put: typeof Put;
	Post: typeof Post;
	Patch: typeof Patch;
	Delete: typeof Delete;
	// Header: typeof Header;
	// Query: typeof Query;
	// Param: typeof Param;
	// Body: typeof Body;
}

export const Route: IRouteFn = function (opts: HandlerOptions) {
	return SetRouteMetadata(opts);
};

Route.Get = Get;
Route.Put = Put;
Route.Post = Post;
Route.Patch = Patch;
Route.Delete = Delete;

// export function defineRoutes(fastify: FastifyInstance, router: object): void {
// 	if (Object.hasOwn(router, 'fastify')) {
// 		Object.defineProperty(router, 'fastify', {
// 			value: fastify,
// 		});
// 	}
// 	if (Object.hasOwn(router, 'logger')) {
// 		Object.defineProperty(router, 'logger', {
// 			value: fastify.log.child({ name: router.constructor.name }),
// 		});
// 	}
// 	const routes = Reflect.getMetadata(ROUTES_METADATA, router) as RouteData;
// 	if (routes) {
// 		for (const method in routes) {
// 			const opts = routes[method];
// 			opts.handler = opts.handler.bind(router as unknown as FastifyInstance);
// 			fastify.route(opts);
// 		}
// 		Reflect.deleteMetadata(ROUTES_METADATA, router);
// 	}
// }

export function Router(options: IRouterOptions = {}) {
	return function <T extends { new (...args: unknown[]): object }>(
		_: T,
		context: ClassDecoratorContext<T>,
	): void | (T & Record<symbol, object>) {
		if (!context.metadata) {
			Object.defineProperty(context, 'metadata', {
				configurable: false,
				writable: false,
				value: {
					prefix: options?.prefix ?? '',
					routes: [],
					routeMap: new Map(),
				},
			});
		} else if (!context.metadata?.routes) {
			Object.assign(context.metadata, {
				prefix: options?.prefix ?? '',
				routes: [],
				routeMap: new Map(),
			});
		} else {
			Object.assign(context.metadata, {
				prefix: options?.prefix ?? '',
			});
		}
	};
}

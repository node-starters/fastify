import fp from 'fastify-plugin';
import type { FastifyInstance, FastifyPluginOptions } from 'fastify';
export * from './router.js';
export * from './auth.js';
import type { IMetadata } from './types.js';

export const fastifyRouter = fp(function (
	fastify: FastifyInstance,
	options: FastifyPluginOptions,
	done: (err?: Error) => void,
): void {
	// eslint-disable-next-line @typescript-eslint/naming-convention
	fastify.decorate('router', function <R extends IRouter>(Router: R) {
		const metadata = (
			Router as unknown as Record<symbol, Record<keyof InstanceType<R>, object>>
		)[Symbol.metadata] as IMetadata;
		new Router();
		if (metadata?.routes) {
			for (const route of metadata.routes) {
				fastify.route(route);
			}
		}
		return fastify;
	});
	done();
});

export interface IRouter {
	new (): unknown;
}
declare module 'fastify' {
	interface FastifyInstance {
		router: (Router: IRouter) => FastifyInstance;
	}
}

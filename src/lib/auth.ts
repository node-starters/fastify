/* eslint-disable @typescript-eslint/indent */
/* eslint-disable @typescript-eslint/naming-convention */
import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import type { IMetadata } from './types.js';
import type { OpenAPIV2 } from 'openapi-types';

export interface IBasicAuth {
	scheme: 'Basic';
}

export interface IBearerAuth {
	scheme: 'Bearer';
}

export type IAuthOptions = IBasicAuth | IBearerAuth;

type IMethodDecorator = <
	This,
	Args extends [FastifyRequest, FastifyReply, FastifyInstance],
	Return,
>(
	handler: (this: This, ...args: Args) => Return,
	context: ClassMethodDecoratorContext<
		This,
		(this: This, ...args: Args) => Return
	>,
) => void;

export function SetAuthMetadata(opts: IAuthOptions): IMethodDecorator {
	return <
		This,
		Args extends [FastifyRequest, FastifyReply, FastifyInstance],
		Return,
	>(
		handler: (this: This, ...args: Args) => Return,
		context: ClassMethodDecoratorContext<
			This,
			(this: This, ...args: Args) => Return
		>,
	): void => {
		if (context.kind !== 'method') {
			throw new Error('Route decorator can be applied on method only');
		}
		if (context.private) {
			throw new Error(
				`'bound' cannot decorate private properties like ${context.name as string}.`,
			);
		}
		context.addInitializer(function () {
			if (!context.metadata?.routes) {
				throw new Error('Not a router class');
			}
			const self = this as Record<
				string | symbol,
				(
					req: FastifyRequest,
					reply: FastifyReply,
					app: FastifyInstance,
				) => unknown
			>;
			const metadata = context.metadata as unknown as IMetadata;
			const security = {
				[opts.scheme.toLowerCase()]: [],
			};
			if (metadata.routeMap.has(context.name)) {
				const index = metadata.routeMap.get(context.name) as number;
				const route = metadata.routes[index];
				if (!route.config) {
					route.config = { auth: opts };
				} else {
					route.config.auth = opts;
				}
				if (!route.schema) {
					route.schema = {
						security: [security],
					};
				} else if (!route.schema.security) {
					route.schema.security = [security];
				} else if (route.schema.security) {
					(route.schema.security as unknown as object[]).push(security);
				}
			} else {
				metadata.routeMap.set(
					context.name,
					metadata.routes.push({
						method: 'GET',
						url: '/',
						handler(req: FastifyRequest, reply: FastifyReply) {
							return self[context.name](req, reply, this);
						},
						config: {
							auth: opts,
						},
						schema: {
							security: [security],
						},
					}) - 1,
				);
			}
		});
	};
}

interface IBasicFn {
	(): IMethodDecorator;
	swagger: OpenAPIV2.SecuritySchemeObject;
}

const Basic: IBasicFn = function (): IMethodDecorator {
	return SetAuthMetadata({
		scheme: 'Basic',
	});
};

Basic.swagger = {
	description: 'Basic Auth',
	type: 'basic',
};

interface IBearerFn {
	(): IMethodDecorator;
	swagger: OpenAPIV2.SecuritySchemeObject;
}

const Bearer: IBearerFn = function (): IMethodDecorator {
	return SetAuthMetadata({
		scheme: 'Bearer',
	});
};

Bearer.swagger = {
	description: 'Bearer Auth',
	type: 'apiKey',
	in: 'header',
	name: 'authorization',
};

interface IAuthFn {
	(opts: IAuthOptions): IMethodDecorator;
	Basic: typeof Basic;
	Bearer: IBearerFn;
}

export const Auth: IAuthFn = function (opts: IAuthOptions) {
	return SetAuthMetadata(opts);
};

Auth.Basic = Basic;
Auth.Bearer = Bearer;

declare module 'fastify' {
	interface FastifyContextConfig {
		auth?: IAuthOptions;
	}
}

/* eslint-disable @typescript-eslint/indent */
import type { FastifySchema, RouteOptions } from 'fastify';

export type HandlerOptions = Omit<RouteOptions, 'handler' | 'schema'> & {
	schema?: Omit<FastifySchema, 'security'>;
};

export type MethodOptions = Omit<HandlerOptions, 'method' | 'url'>;

export interface IRouterOptions {
	prefix?: string;
}

export interface IMetadata extends IRouterOptions {
	routes: RouteOptions[];
	routeMap: Map<string | symbol, number>;
}

export type IRouterInstance = Record<
	string | symbol,
	(req: FastifyRequest, reply: FastifyReply, app: FastifyInstance) => unknown
>;

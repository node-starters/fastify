import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';

export async function verifyAuth(
	this: FastifyInstance,
	req: FastifyRequest,
	reply: FastifyReply,
): Promise<void> {
	const { auth } = req.routeOptions.config;
	if (!auth) {
		return;
	}
	try {
		const header = req.headers.authorization;
		if (!header) {
			throw new Error('Authorization is missing');
		}
		const [scheme, token] = header.split(' ');
		if (scheme !== auth.scheme) {
			throw new Error('Authorization scheme is invalid');
		}
		if (scheme === 'Basic') {
			const buffer = Buffer.from(token, 'base64');
			const [user, pass] = buffer.toString('binary').split(':');
			if (
				user !== this.config.BASIC_AUTH_USER ||
				pass !== this.config.BASIC_AUTH_PASS
			) {
				throw new Error('Authorization credential is invalid');
			}
		} else if (scheme === 'Bearer') {
			// verify jw
		}
	} catch (err) {
		console.info(err);
		await reply
			.status(401)
			.header('www-authenticate', `${auth.scheme} realm=humblx`)
			.send({ message: (err as Error).message });
	}
}

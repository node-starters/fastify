import { fastify } from 'fastify';
import gracefulShutdown from 'close-with-grace';
import env from '@fastify/env';
import schema from './config/env.json' assert { type: 'json' };
import { defineSchema } from './schema/index.js';

process.addListener('uncaughtException', (err) => {
	console.error(err);
});

export const app = await fastify({
	logger: process.env.NODE_ENV !== 'local' || {
		transport: {
			target: 'pino-pretty',
			options: {
				translateTime: 'HH:MM:ss Z',
				ignore: 'pid,hostname',
			},
		},
	},
});

gracefulShutdown(async () => await app.close());

await app.register(env, { schema, dotenv: true });

// register common json schemas
defineSchema(app);
